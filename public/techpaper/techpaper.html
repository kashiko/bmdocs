<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="stylesheet" href="style.css" />
<title>Bitmessage Technical Paper Rev1</title>
</head>
<body>

<h1><a id="Proposed Bitmessage Protocol Technical Paper">
Proposed Bitmessage Protocol Technical Paper
</a></h1>
<p>
Jonathan Warren<br />
bitmessage@jonwarren.org<br />
<br />
Revision 1<br />
January 14, 2013
</p>
<p>
<strong>Abstract.</strong> The purpose of this paper is to help
researchers analyze and critique the Bitmessage protocol
before an implementation is completed. Comments and
suggestions are welcome.
</p>

<h2><a id="1. Introduction">
1. Introduction
</a></h2>
<p>
Bitmessage is a proposed P2P communications protocol used to send email-like messages to another
person or to many subscribers. Non-technical details about how the system would work are described in
another document available at: <a href="http://bitmessage.org/bitmessage.pdf">http://bitmessage.org/bitmessage.pdf</a><br />
With this document and linked source code files, we aim to describe the protocol in enough detail to
allow researchers to critique its security.
</p>

<h2><a id="2. Goals">
2. Goals
</a></h2>
<p>
The goal of the project is to develop a messaging protocol with the following features:
</p>
<ul>
<li>Decentralized</li>
<li>Trustless</li>
<li>Messages well-encrypted</li>
<li>Messages authenticated</li>
<li>Not require users to exchange and manage keys</li>
<li>Hide “non-content” data like the sender and receiver of messages from eavesdroppers. This may
be difficult to accomplish.</li>
</ul>

<h2><a id="3. Proposed solution">
3. Proposed solution
</a></h2>
<p>
To accomplish these goals, we propose that nodes of a P2P network exchange messages in the same way
that Bitcoin nodes exchange transactions: by forwarding them on a best effort basis. Just like with Bitcoin
transactions, all nodes will receive all messages. This is meant to hide the receiver of a message, as
receiving a message can be a passive process. In practice, however, receiving a message is not passive as
the receiver will usually send an acknowledgement. Below we also discuss possible methods of
countering attackers who eavesdrop on individual users’ Internet connections in order to find out if they
are the sender or receiver of a message, or are the owner of a particular identity.
</p>
<p>
Messages should be signed by the sender of a message and then encrypted for the receiver of the
message. Each node will be responsible for attempting to decrypt each message with each of their private
keys. To limit the number and size of messages flowing through the network, a proof-of-work scheme is
incorporated. A positive side-effect is that this may also limit spam.
</p>
<p>
Public keys and requests for public keys are exchanged in the same way that messages are exchanged: by
forwarding them through the network. Addresses exchanged by humans are a base58-encoded-hash of
their public keys. To send a message, a Bitmessage client requests the public key based on the hash and
waits for the public key to arrive through the network. After it does, it can use the public key to encrypt
the message bound for the recipient.<br />
In order to scale, nodes self divide into <em>streams</em>. This is discussed in more detail in the other document,
<a href="http://bitmessage.org/bitmessage.pdf">http://bitmessage.org/bitmessage.pdf</a>.<br />
Because all nodes receive all messages, a natural extension of the protocol is to support broadcasting
messages to subscribers. Users may subscribe to an address through their user interface and any broadcast
type messages sent through the network by that address will be displayed to the user. Receiving a
broadcast is an entirely passive process.<br />
Thus there are four types of ‘objects’ that are propagated throughout a Bitmessage stream: getpubkey,
pubkey, msg, and broadcast.
</p>

<h2><a id="4. Interaction Between Nodes">
4. Interaction Between Nodes
</a></h2>
<p>
Every message sent between nodes has this message header:
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>4</td>
<td>magic</td>
<td>uint32_t</td>
<td>Magic value indicating message origin network, and used to seek to next message when stream state is unknown</td>
</tr>
<tr>
<td>12</td>
<td>command</td>
<td>char[12]</td>
<td>ASCII string identifying the packet content, NULL padded (non-NULL padding results in packet rejected)</td>
</tr>
<tr>
<td>4</td>
<td>length</td>
<td>uint32_t</td>
<td>Length of payload in number of bytes</td>
</tr>
<tr>
<td>4</td>
<td>checksum</td>
<td>uint32_t</td>
<td>First 4 bytes of sha512(payload)</td>
</tr>
<tr>
<td>?</td>
<td>payload</td>
<td>uchar[]</td>
<td>The actual data</td>
</tr>
</table>
<table>
<tr>
<th>Magic value</th>
<th>Sent over wire as</th>
</tr>
<tr>
<td>0xE9BEB4D9</td>
<td>E9 BE B4 D9</td>
</tr>
</table>
<p>
After connecting to a node using TCP, the initiator sends a version message that describes the version of
the protocol it is using to the other node. If the other node accepts, it sends a verack packet. The node
receiving the incoming connection then repeats the same process itself.
</p>
<p>
<img src="fig1.png" alt="Figure 1" />
</p>
<p>
The version message has this format (along with the header above):
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>4</td>
<td>version</td>
<td>int32_t</td>
<td>Identifies protocol version being used by the node</td>
</tr>
<tr>
<td>8</td>
<td>services</td>
<td>uint64_t</td>
<td>bitfield of features to be enabled for this connection</td>
</tr>
<tr>
<td>8</td>
<td>timestamp</td>
<td>int64_t</td>
<td>standard UNIX timestamp in seconds</td>
</tr>
<tr>
<td>26</td>
<td>addr_recv</td>
<td>net_addr</td>
<td>The network address of the node receiving this message</td>
</tr>
<tr>
<td>26</td>
<td>addr_from</td>
<td>net_addr</td>
<td>The network address of the node emitting this message</td>
</tr>
<tr>
<td>8</td>
<td>nonce</td>
<td>uint64_t</td>
<td>Random nonce used to detect connections to self.</td>
</tr>
<tr>
<td>1+</td>
<td>user_agent</td>
<td><strong>var_str</strong></td>
<td><strong>User Agent</strong> (0x00 if string is 0 bytes long)</td>
</tr>
<tr>
<td>1+</td>
<td>stream numbers</td>
<td><strong>var_int_list</strong></td>
<td>The stream numbers that the emitting node is interested in.</td>
</tr>
</table>
<p>
This structure uses the following var_int_list, var_str, and net_addr structures:
</p>
<p>
<strong>Variable length integer</strong>
</p>
<p>
Integer can be encoded depending on the represented value to save space.
Variable length integers always precede an array/vector of a type of data that may vary in length.
</p>
<table>
<tr>
<th>Value</th>
<th>Storage length</th>
<th>Format</th>
</tr>
<tr>
<td>&lt; 0xfd</td>
<td>1</td>
<td>uint8_t</td>
</tr>
<tr>
<td>&lt;= 0xffff</td>
<td>3</td>
<td>0xfd followed by the length as uint16_t</td>
</tr>
<tr>
<td>&lt;= 0xffffffff</td>
<td>5</td>
<td>0xfe followed by the length as uint32_t</td>
</tr>
<tr>
<td>-</td>
<td>9</td>
<td>0xff followed by the length as uint64_t</td>
</tr>
</table>
<p>
<strong>Variable length string</strong>
</p>
<p>
Variable length string can be stored using a variable length integer followed by the string itself.
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>1+</td>
<td>length</td>
<td><strong>var_int</strong></td>
<td>Length of the string</td>
</tr>
<tr>
<td>?</td>
<td>string</td>
<td>char[]</td>
<td>The string itself (can be empty)</td>
</tr>
</table>
<p>
<strong>Variable length list of integers</strong>
</p>
<p>
n integers can be stored using n+1 <strong>variable length integers</strong> where the first var_int equals n.
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>1+</td>
<td>count</td>
<td><strong>var_int</strong></td>
<td>Number of var_ints below</td>
</tr>
<tr>
<td>1+</td>
<td></td>
<td>var_int</td>
<td>The first value stored</td>
</tr>
<tr>
<td>1+</td>
<td></td>
<td>var_int</td>
<td>The second value stored...</td>
</tr>
<tr>
<td>1+</td>
<td></td>
<td>var_int</td>
<td>etc...</td>
</tr>
</table>
<p>
<strong>Network Address</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>4</td>
<td>time</td>
<td>uint32</td>
<td>the Time</td>
</tr>
<tr>
<td>4</td>
<td>stream</td>
<td>uint32</td>
<td>Stream number for this node</td>
</tr>
<tr>
<td>8</td>
<td>services</td>
<td>uint64_t</td>
<td>same service(s) listed in <strong>version</strong></td>
</tr>
<tr>
<td>16</td>
<td>IPv6/4</td>
<td>char[16]</td>
<td>IPv6 address. The original client only supports IPv4 and only reads the last 4 bytes to get the IPv4 address. However, the IPv4 address is written into the message as a 16 byte <strong>IPv4-mapped IPv6 address</strong><br />(12 bytes <em>00 00 00 00 00 00 00 00 00 00 FF FF</em>, followed by the 4 bytes of the IPv4 address).</td>
</tr>
<tr>
<td>2</td>
<td>port</td>
<td>uint16_t</td>
<td>port number</td>
</tr>
</table>
<p>
The <strong>verack</strong> message is blank: it is just the message header above with “verack” as the command.<br />
Now that the connection is fully established, each node should advertise this new node’s address to its
peers in an <strong>addr</strong> message:
</p>
<p>
<strong>addr</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>1+</td>
<td>count</td>
<td><strong>var_int</strong></td>
<td>Number of address entries (max: 1000)</td>
</tr>
<tr>
<td>30x?</td>
<td>addr_list</td>
<td><strong>net_addr</strong></td>
<td>Address of other nodes on the network.</td>
</tr>
</table>
<p>
It should also send a large addr message to the new peer listing random peers of which it is aware to help
it become a better connected node. Thus far, this protocol is almost exactly that used by Bitcoin.<br />
At this point, each node should send an inventory (<strong>inv</strong>) message listing the objects of which it is aware.
</p>
<p>
<strong>inv</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>?</td>
<td>count</td>
<td><strong>var_int</strong></td>
<td>Number of inventory entries</td>
</tr>
<tr>
<td>32x?</td>
<td>inventory</td>
<td><strong>inv_vect</strong>[]</td>
<td>Inventory vectors</td>
</tr>
</table>
<p>
This structure references inventory vectors. Inventory vectors are used for notifying other nodes about
objects they have or data which is being requested. Two rounds of SHA-512 are used, resulting in a 64
byte hash. Only the first 32 bytes are used; the later 32 bytes are ignored. The rationale for not using SHA-
256 is that Bitcoin uses SHA-256 throughout, including for the proof-of-work for blocks. Bitmessage
should use a different algorithm so that using Bitcoin mining hardware to do Bitmessage POWs is at least
not completely trivial. SHA-512 is used throughout Bitmessage (except for certain places within the
encryption implementation). Changing the POW algorithm to one designed to run “poorly” on GPUs and
custom hardware would be a positive change but rapidly developing GPGPU hardware makes it difficult
to judge what algorithms will be appropriate in the future.
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>32</td>
<td>hash</td>
<td>char[32]</td>
<td>Hash of the object</td>
</tr>
</table>
<p>
<strong>getdata</strong> is used in response to an <strong>inv</strong> message to retrieve the content of a specific object after filtering
known elements.<br />
Payload (maximum payload length: 50000 entries)
</p>
<p>
<strong>getdata</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>?</td>
<td>count</td>
<td><strong>var_int</strong></td>
<td>Number of inventory entries</td>
</tr>
<tr>
<td>32x?</td>
<td>inventory</td>
<td><strong>inv_vect</strong>[]</td>
<td>Inventory vectors</td>
</tr>
</table>
<p>
The peer should then send the requested object in a <strong>getpubkey</strong>, <strong>pubkey</strong>, <strong>msg</strong>, or <strong>broadcast</strong> message
depending on the type of the requested object.
</p>
<p>
<img src="fig2.png" alt="Figure 2" />
</p>
<p>
Using this model, Client 1 downloads all objects stored by Client 2. We propose that each client store
objects for about two days and then delete them to reclaim disk space. If the receiver of a message is not
online to receive it during the two-day window, the sender will notice that he never received an
acknowledgement and will resend the message after waiting an exponentially increasing amount of time.
</p>

<h2><a id="5. Sending a message">
5. Sending a message
</a></h2>
<p>
(Pseudocode is below)
</p>
<p>
<img src="fig3.png" alt="Figure 3" />
</p>
<p>
<strong>Informal description:</strong><br />
For Alice to send a message to Bob, Bob must use a trusted medium to give her his address which is a
base58-encoded RIPEMD160 hash of two secp256k1 public keys, one used for signing and the other used
for encryption. Alice broadcasts out a request to get the public keys. Upon seeing the request, Bob
broadcasts out his public keys which are then stored by all nodes in case they also want to send a
message to Bob or if another node requests them. When Alice receives the public keys, she uses her
private signing key to sign a message to Bob, then uses Bob’s public encryption key to encrypt the
message. She then broadcasts the message throughout the Bitmessage stream. Each node attempts to
decrypt the message with each of their private encryption keys and also passes the message to peers. Bob
decrypts the message then checks the signature using the public signing key included in the message.
Finally, he hashes the public key in order to create the base58 address to display in the user interface. By
default, he will also send an acknowledgement which is also included in the message from Alice.
</p>
<p>
Sending a broadcast is simpler: Alice signs a message and broadcasts out her public keys, the message,
and the signature in a <strong>broadcast</strong> message. Any nodes that wish to display it may do so.
</p>
<p>
The formats of each of the four objects types are as follows:
</p>
<p>
<strong>getpubkey</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>8</td>
<td>POW nonce</td>
<td>uint64_t</td>
<td>Random nonce used for the <strong>Proof Of Work</strong></td>
</tr>
<tr>
<td>4</td>
<td>time</td>
<td>uint32_t</td>
<td>The time that this message was generated and broadcast</td>
</tr>
<tr>
<td>1+</td>
<td>address version</td>
<td>var_int</td>
<td>The address' version</td>
</tr>
<tr>
<td>1+</td>
<td>stream number</td>
<td>var_int</td>
<td>The address' stream number</td>
</tr>
<tr>
<td>20</td>
<td>pub key hash</td>
<td>uchar[]</td>
<td>The ripemd hash of the public key</td>
</tr>
<tr>
<td></td>
</tr>
</table>
<p>
<strong>pubkey</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>8</td>
<td>POW nonce</td>
<td>uint64_t</td>
<td>Random nonce used for the <strong>Proof Of Work</strong></td>
</tr>
<tr>
<td>4</td>
<td>time</td>
<td>uint32_t</td>
<td>The time that this message was generated and broadcast.*</td>
</tr>
<tr>
<td>1+</td>
<td>address version</td>
<td>var_int</td>
<td>The address' version</td>
</tr>
<tr>
<td>1+</td>
<td>stream number</td>
<td>var_int</td>
<td>The address' stream number</td>
</tr>
<tr>
<td>4</td>
<td><strong>behavior bitfield</strong></td>
<td>uint32_t</td>
<td>A bitfield of optional behaviors and features that can be expected from the node receiving the message.</td>
</tr>
<tr>
<td>64</td>
<td>public signing key</td>
<td>uchar[]</td>
<td>The ECC public key used for signing (uncompressed format; normally prepended with \x04 )</td>
</tr>
<tr>
<td>64</td>
<td>public encryption key</td>
<td>uchar[]</td>
<td>The ECC public key used for encryption (uncompressed format; normally prepended with \x04 )</td>
</tr>
</table>
<p>
<strong>msg</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>8</td>
<td>POW nonce</td>
<td>uint64_t</td>
<td>Random nonce used for the <strong>Proof Of Work</strong></td>
</tr>
<tr>
<td>4</td>
<td>time</td>
<td>uint32_t</td>
<td>The time that this message was generated and broadcast</td>
</tr>
<tr>
<td>1+</td>
<td>streamNumber</td>
<td>var_int</td>
<td>The stream number of the destination address.</td>
</tr>
<tr>
<td>?</td>
<td>encrypted</td>
<td>uchar[]</td>
<td>Encrypted data. See also <strong>Unencrypted Message Data Format</strong></td>
</tr>
</table>
<p>
<strong>broadcast</strong>
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>8</td>
<td>POW nonce</td>
<td>uint64_t</td>
<td>The <strong>Proof Of Work</strong> nonce</td>
</tr>
<tr>
<td>4</td>
<td>time</td>
<td>uint32_t</td>
<td>The time that the message was broadcast</td>
</tr>
<tr>
<td>1+</td>
<td>broadcast version</td>
<td>var_int</td>
<td>The version number of this broadcast protocol message.</td>
</tr>
<tr>
<td>1+</td>
<td>address version</td>
<td>var_int</td>
<td>The sender's address version</td>
</tr>
<tr>
<td>1+</td>
<td>stream number</td>
<td>var_int</td>
<td>The sender's stream number</td>
</tr>
<tr>
<td>4</td>
<td><strong>behavior bitfield</strong></td>
<td>uint32_t</td>
<td>A bitfield of optional behaviors and features that can be expected from the node receiving the message.</td>
</tr>
<tr>
<td>64</td>
<td>public signing key</td>
<td>uchar[]</td>
<td>The ECC public key used for signing (uncompressed format; normally prepended with \x04 )</td>
</tr>
<tr>
<td>64</td>
<td>public encryption key</td>
<td>uchar[]</td>
<td>The ECC public key used for encryption (uncompressed format; normally prepended with \x04 )</td>
</tr>
<tr>
<td>20</td>
<td>address hash</td>
<td>uchar[]</td>
<td>The sender's address hash. This is included so that nodes can more cheaply detect whether this is a broadcast message for which they are listening, although it must be verified with the public key above.</td>
</tr>
<tr>
<td>1+</td>
<td><strong>encoding</strong></td>
<td>var_int</td>
<td>The <strong>encoding type</strong> of the message</td>
</tr>
<tr>
<td>1+</td>
<td>messageLength</td>
<td>var_int</td>
<td>The message length in bytes</td>
</tr>
<tr>
<td>messageLength</td>
<td>message</td>
<td>uchar[]</td>
<td>The message</td>
</tr>
<tr>
<td>1+</td>
<td>sig_length</td>
<td>var_int</td>
<td>Length of the signature</td>
</tr>
<tr>
<td>sig_length</td>
<td>signature</td>
<td>uchar[]</td>
<td>The signature which covers everything from the time down through the message.</td>
</tr>
</table>
<p>
msg messages contain encrypted data. The unencrypted data format is:
</p>
<table>
<tr>
<th>Field Size</th>
<th>Description</th>
<th>Data type</th>
<th>Comments</th>
</tr>
<tr>
<td>1+</td>
<td>msg_version</td>
<td>var_int</td>
<td>Message format version</td>
</tr>
<tr>
<td>1+</td>
<td>address_version</td>
<td>var_int</td>
<td>Sender's address version number. This is needed in order to calculate the sender's address to show in the UI, and also to allow for forwards compatible changes to the public-key data included below.</td>
</tr>
<tr>
<td>1+</td>
<td>stream</td>
<td>var_int</td>
<td>Sender's stream number</td>
</tr>
<tr>
<td>4</td>
<td>behavior bitfield</td>
<td>uint32_t</td>
<td>A bitfield of optional behaviors and features that can be expected from the node with this pubkey included in this msg message (the sender's pubkey).</td>
</tr>
<tr>
<td>64</td>
<td>public signing key</td>
<td>uchar[]</td>
<td>The ECC public key used for signing (uncompressed format; normally prepended with \x04 )</td>
</tr>
<tr>
<td>64</td>
<td>public encryption key</td>
<td>uchar[]</td>
<td>The ECC public key used for encryption (uncompressed format; normally prepended with \x04 )</td>
</tr>
<tr>
<td>20</td>
<td>destination ripe</td>
<td>uchar[]</td>
<td>The ripe hash of the public key of the receiver of the message</td>
</tr>
<tr>
<td>1+</td>
<td>encoding</td>
<td>var_int</td>
<td><strong>Message Encoding</strong> type</td>
</tr>
<tr>
<td>1+</td>
<td>message_length</td>
<td>var_int</td>
<td>Message Length</td>
</tr>
<tr>
<td>message_length</td>
<td>message</td>
<td>uchar[]</td>
<td>The message.</td>
</tr>
<tr>
<td>1+</td>
<td>ack_length</td>
<td>var_int</td>
<td>Length of the acknowledgement data</td>
</tr>
<tr>
<td>ack_length</td>
<td>ack_data</td>
<td>uchar[]</td>
<td>The acknowledgement data to be transmitted. This takes the form of a Bitmessage protocol message, like another msg message. The POW therein must already be completed.</td>
</tr>
<tr>
<td>1+</td>
<td>sig_length</td>
<td>var_int</td>
<td>Length of the signature</td>
</tr>
<tr>
<td>sig_length</td>
<td>signature</td>
<td>uchar[]</td>
<td>The ECDSA signature of the destination ripe, encoding, message_length, message, ack_length, and ack_data all appended.</td>
</tr>
</table>
<p>
<strong>Pseudocode</strong><br />
To calculate an address:<br />
</p>
<div class="indent">
  <p>
If using a PRNG:
  </p>
  <div class="indent">
    <p>
private_signing_key = random 32 byte string
    </p>
    <p>
private_encryption_key = random 32 byte string
    </p>
  </div>
  <p>
Else if calculating an address deterministically using a passphrase:
  </p>
  <div class="indent">
    <p>
private_signing_key = first 32 bytes of SHA512 ( passphrase || “\x00” )
    </p>
    <p>
private_encryption_key = first 32 bytes of SHA512 ( passphrase || “\x01” )
    </p>
  </div>
  <p>
public_signing_key = calculate public key from private_signing_key
  </p>
  <p>
public_encryption_key = calculate public key from private_encryption_key
  </p>
  <p>
hash = RIPEMD160 ( SHA512 (public_signing_key || public_encryption_key )
  </p>
  <p>
checksum = first four bytes of SHA512 ( SHA512 ( address version || stream number || hash ) )
  </p>
  <p>
address = base58encode ( address version || stream number || hash || checksum )
  </p>
</div>
<p>
To send a message:
</p>
<div class="indent">
  <p>
Check that the destination address is valid by checking the checksum
  </p>
  <p>
Check if we already have the recipient’s public key. If we do not:
  </p>
  <div class="indent">
    <p>
assemble a getpubkey message and do the necessary proof-of-work
    </p>
    <p>
for each peer to whom we are connected:
    </p>
    <div class="indent">
      <p>
wait a random amount of time from 0 to 10 seconds
      </p>
      <p>
send the getpubkey message
      </p>
    </div>
  </div>
  <p>
Wait for Bob to respond with his public key. After 4 days, make the request again (then again in 8
days, then 16 days….)
  </p>
  <p>
After you receive Bob’s public key:
  </p>
  <div class="indent">
    <p>
payload = time || stream number || 32 bytes of random data
    </p>
    <p>
complete the proof of work for this payload and attach the nonce and msg header to the
front of the payload. This payload is the acknowledgement data
    </p>
    <p>
store the payload in a data structure (this will later be checked by the receive_msg
function in order to detect when the acknowledgement arrives.)
    </p>
    <p>
Assemble together a new msg message
    </p>
    <p>
Sign the message with your private signing key using ECDH and attach the signature
(actual Python code below for this signing step)
    </p>
    <p>
Encrypt the message with the receiver’s public encryption key using ECIES (actual
Python code below for this step)
    </p>
    <p>
Complete the necessary proof-of-work
    </p>
    <p>
For each peer to whom we are connected,
    </p>
    <div class="indent">
      <p>
wait a random amount of time between 0 and 10 seconds
      </p>
      <p>
send the msg
      </p>
    </div>
  </div>
</div>
<p>
For each msg message we receive:
</p>
<div class="indent">
  <p>
Check the proof-of-work. Abort if insufficient (and move on to the next message if there is one).
  </p>
  <p>
Check the time. Abort if more than 2.5 days in the past or three hours in the future.
  </p>
  <p>
Check stream number. Abort if it is not the stream associated with this connection.
  </p>
  <p>
For each of our peers:
  </p>
  <div class="indent">
    <p>
Wait a random amount of time between 0 and 10 seconds
    </p>
    <p>
Broadcast the msg message
    </p>
  </div>
  <p>
Check whether this is an acknowledgement bound for us by looking up the msg data in our
awaiting_ack_data data structure.
  </p>
  <p>
For each of our private encryption keys:
  </p>
  <div class="indent">
    <p>
Try to decrypt the data. (Actual Python code below.) If decryption is successful:
    </p>
    <div class="indent">
      <p>
Verify that the message version = 1. Abort if not.
      </p>
      <p>
Verify that the sender’s address version is one we understand. Abort if not.
      </p>
      <p>
Verify that the destination_ripe matches the ripe hash of our public keys. Abort if
not.
      </p>
      <p>
Verify the validity of the signature with the public signing key included in the
message. The signature covers the ripe hash of the receiver’s public keys,
encoding type, message_length, message, ack_length, and ackData, all
appended. Abort if signature check fails.
      </p>
      <p>
Store the sender’s public keys for optional later use.
      </p>
      <p>
If we have not received this message before, display the message.
      </p>
      <p>
Check that then length of the ackData is greater than 24. Abort if not.
      </p>
      <p>
Check that the magic bytes on the ackData message are correct. Abort if not.
      </p>
      <p>
Check that the encoded payload length in the ackData message matches the
actual length. Abort if not.
      </p>
      <p>
Add the ackData to a list of messages to process once all other messages from
this peer have been processed. We will process it by following this same
function. By default, we, of course, won’t be able to decrypt it because it is 32
bytes of random data, but users may use actual objects (msg, broadcast,
getpubkey, or pubkey messages) as ackData.
      </p>
    </div>
    <p>
Else if the decryption is not successful:
    </p>
    <div class="indent">
      <p>
Sleep for a calculated amount of time (0.3 to several seconds depending on the
size of the message and the speed of your computer). Thus decrypting or
failing to decrypt the message will take the same amount of time.
      </p>
    </div>
  </div>
</div>
<p>
Algorithm to calculate the proof-of work:
</p>
<div class="indent">
  <p>
nonce = 0
  </p>
  <p>
averageProofOfWorkNonceTrialsPerByte = 320
  </p>
  <p>
extraBytes = 14000
  </p>
  <p>
trialValue = 99999999999999999999
  </p>
  <p>
target = 2<sup>64</sup> / ((length of the payload + extraBytes ) * averageProofOfWorkNonceTrialsPerByte)
  </p>
  <p>
initialHash = SHA512 (payload)
  </p>
  <p>
while trialValue &gt; target:
  </p>
  <div class="indent">
    <p>
nonce += 1
    </p>
    <p>
trialValue = first 8 bytes of SHA512( SHA512( nonce byte string + initialHash) )
interpreted as int
    </p>
    <p>
prepend the <em>nonce byte string</em> to the payload
    </p>
  </div>
</div>

<h2><a id="6. Encryption Source Code">
6. Encryption Source Code
</a></h2>
<p>
To implement ECIES and ECDH, we rely on a Python wrapper for OpenSSL.<br />
The highest level functions are here:<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/bitcoin.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/bitcoin.py</a>
</p>
<p>
This requires several other files:
</p>
<p>
openssl.py<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/openssl.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/openssl.py</a>
</p>
<p>
ecc.py<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/ecc.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/ecc.py</a>
</p>
<p>
cipher.py<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/cipher.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/cipher.py</a>
</p>
<p>
hash.py<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/hash.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/hash.py</a>
</p>
<p>
arithmetic.py<br />
<a href="https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/arithmetic.py">https://github.com/Atheros1/pyelliptic/blob/master/pyelliptic/arithmetic.py</a>
</p>

<h2><a id="7. Attackers">
7. Attackers
</a></h2>
<p>
There are several attackers against whom we hope to defend:
</p>
<ul>
<li>Chuck is malicious but has no abilities above normal Internet nodes.</li>
<li>NSA may eavesdrop on Internet backbones but not individual Internet connections.</li>
<li>Eve may eavesdrop on a particular Internet connection but not all individual Internet connections.</li>
<li>Mallory is malicious, has the abilities of Eve, and may also modify and drop packets at-will.</li>
</ul>
<p>
Alice sends a message to Bob. Both are honest.
</p>
<p>
Concerning the first five goals of the project (decentralized, trustless, encrypted, authenticated, and
simple exchange of identity information), we contend that they all can be accomplished using the
methods above although when Mallory is the attacker, he can prevent individuals from communicating
by blocking all messages to and from the target node. Exchanging hashes of public keys guarantees that
users receive the correct public keys, and at that point the security of the system should be no different
than with other public key systems.
</p>
<p>
The last goal, hiding the sender and receiver of individual messages, is much more difficult.
</p>

<h2><a id="8. Attacks">
8. Attacks
</a></h2>
<p>
<strong>Eavesdropping attack</strong><br />
We contend that NSA, under the definition above, cannot identify the sender or receiver of a message but
can identify the rough geographic location of Alice and Bob with a reasonable probability. To find the
locations or, perhaps, the real life identities of Alice or Bob, he would need to monitor all individual
Internet connections in order to detect which node first responds to an acknowledgement. None of our
attackers have this ability although Eve may perform the attack on individual nodes that she suspects
may harbor Alice or Bob. This attack is viable.<br />
We finally contend that Mallory is no more an able attacker than Eve except that he may block messages.
Someday, when the bitfield_features field is used, that field might need to be signed within the pubkey
message if it can be imagined that flipping bits in the field would be of any particular benefit or nuisance
to anyone.<br />
<strong>Proposed solution</strong><br />
The eavesdropping attack can be thwarted if Bob, being a paranoid individual, instead of sending an
acknowledgement out through his own Internet connection, waits a random number of minutes and then
packages it up within another message and sends it to yet another user, Charlie, a public figure whom he
trusts not to be colluding with Eve. The message Bob sends to Charlie may even be a normal important
message (rather than a blank message). When Charlie broadcasts the acknowledgement, it would
simultaneously acknowledge the message both from Alice to Bob and from Bob to Charlie. If Charlie and
Bob do not personally know each other, Bob may send a blank message which is not shown in Charlie’s
user interface. Bob may also distribute his public keys or send msg or broadcast messages in this manner.
Charlie is happy to provide this service because the marginal cost of sending an acknowledgement is
small and because it gives Charlie plausible deniability for the acknowledgement messages which truly
<em>are</em> his which emanate from his Internet connection. This proposed solution is an attempt to accomplish
one goal (hiding message meta-data from Eve and Mallory) but comes at the cost of another
(trustlessness).<br />
Other suggestions are welcome.
</p>
<p>
<strong>Timing attack</strong><br />
Naively implemented, a Bitmessage client would be vulnerable to a timing attack where Chuck sends a
series of messages to node1 and node2 in an attempt to locate Alice. Chuck sends a hundred msg
messages bound for Alice to node1. Let us suppose that it takes a typical node one second to process an
msg message if the message is not bound for any address owned by this node but two seconds if it is as
the node must decrypt and display it. Node1 would reach and request the last msg message after 100
seconds if Alice is not at the node or 200 seconds if she is. This reveals Alice’s location.<br />
<strong>Proposed Solution</strong><br />
We propose that nodes measure the length of time it takes to successfully decrypt and display messages
of various sizes and also the time it takes to unsuccessfully decrypt messages of various sizes. The
difference is the amount of time a node should sleep if it is unable to decrypt a message. Nodes can come
pre-programmed with reasonable default values and can adjust based on their own timings.
</p>

<h2><a id="9. Extensions">
9. Extensions
</a></h2>
<p>
<strong>Instant Messaging</strong><br />
Using the protocol above, an IM interface would not be functional because the proof-of-work would take
several minutes to complete for each message. We propose an extension to the above protocol where a
special operating mode is used for IM and for sending large attachments. The idea is that nodes agree to
directly connect to one-another or to a third party if they are blocked by firewalls. They then forgo the
POW requirement and are able to send instant messages or files of any size. Using this option, the last
goal of the project is abandoned: both the third party and NSA would be able to tell which two nodes are
communicating. Users would, however, be free to use some Bitmessage addresses privately- never using
the IM operating mode- and others publicly.<br />
Other ideas on how to implement instant messaging are welcome.
</p>

<h2><a id="10. Conclusion">
10. Conclusion
</a></h2>
<p>
We have presented our plan for how a Bitmessage protocol might work. We invite researchers to review
our chosen cryptographic implementation and review our rough specification looking for potential
problems. We are confident that users worldwide would benefit from a protocol like this and we aim to
acquire input and make changes early in the design process to help the project be as successful as it can be.
</p>
<hr />
<p>
[<a href="Bitmessage%20Technical%20Paper.pdf">Original PDF</a>]
Converted to HTML by Kashiko Koibumi.
[<a href="techpaper.txt">Creole</a>]
[<a href="https://kashiko.gitlab.io/licana-gnat/">Licana</a>]
</p>
<p>
<a href="https://bitmessage.org/Bitmessage%20Technical%20Paper.pdf">https://bitmessage.org/Bitmessage%20Technical%20Paper.pdf</a>
<a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0</a>
</p>

</body>
</html>
